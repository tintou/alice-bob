use gtk::prelude::*;
use shumate::prelude::*;

pub struct Window {
    pub widget: gtk::ApplicationWindow,
    pub map: shumate::Map,
    pub scale: shumate::Scale,
    pub compass: shumate::Compass,
    pub license: shumate::License,
}

impl Window {
    pub fn new() -> Self {
        let builder = gtk::Builder::from_resource("/tf/noel/Alice-Bob/window.ui");
        let widget: gtk::ApplicationWindow = builder
            .object("window")
            .expect("Failed to find the window object");
        let map: shumate::Map = builder
            .object("map")
            .expect("Failed to find the map object");
        let scale: shumate::Scale = builder
            .object("scale")
            .expect("Failed to find the scale object");
        let compass: shumate::Compass = builder
            .object("compass")
            .expect("Failed to find the compass object");
        let license: shumate::License = builder
            .object("license")
            .expect("Failed to find the license object");

        let viewport : shumate::Viewport = map.viewport().unwrap();
        scale.set_viewport(Option::Some(&viewport));
        compass.set_viewport(Option::Some(&viewport));

        let registry = shumate::MapSourceRegistry::with_defaults();
        let mapsource = registry.item(0).unwrap().downcast::<shumate::MapSource>().unwrap();

        viewport.set_reference_map_source(Option::Some(&mapsource));
        map.set_map_source(&mapsource);

        let layer = shumate::MapLayer::new(&mapsource, &viewport);
        map.insert_layer_behind(&layer, Option::<&shumate::Layer>::None);
        license.append_map_source(&mapsource);

        Self { widget, map, scale, compass, license }
    }
}
